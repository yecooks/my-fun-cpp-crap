#include <iostream>
using namespace std;

void List(int x,int array[5]){
	cout << "[";
	for (int i=0; i<x; i++){
		cout << array[i];
		if (i!=4){
			cout << ",";
		}
	}
	cout << "]";
	cout << "\n";
}

void Size(int MyName[5],int x){
	for (int i=0; i<x-1; i++) {
		if (MyName[i]>MyName[i+1]) {
			int y=MyName[i+1];
			MyName[i+1]=MyName[i];
			MyName[i]=y;
		}
	}
}

int main(){
	int x=5;
	int MyName[x]={-5,-7,-9,-1,-3};
	for (int i=0; i<x; i++){
		Size(MyName,x);
	}
	List(x,MyName);
	cout << MyName[4];
	cout << "\n";
	return 0;
}

